import 'package:auth_mobile/model/user.dart';
import 'package:flutter/cupertino.dart';

class UserProvider with ChangeNotifier {
  User _user;

  User get user => _user;

  set user(User value) {
    _user = value;
    notifyListeners();
  }
}
