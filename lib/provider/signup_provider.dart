import 'package:flutter/cupertino.dart';

class SignupProvider with ChangeNotifier {
  String _email;
  String _password;
  String _name;

  String get email => _email;

  set email(String value) {
    _email = value;
    notifyListeners();
  }

  String get password => _password;

  set password(String value) {
    _password = value;
    notifyListeners();
  }

  String get name => _name;

  set name(String value) {
    _name = value;
    notifyListeners();
  }
}
