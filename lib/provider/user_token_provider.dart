import 'package:auth_mobile/model/identity_token.dart';
import 'package:flutter/cupertino.dart';

class UserTokenProvider with ChangeNotifier {
  IdentityToken _identityToken;

  IdentityToken get identityToken => _identityToken;

  set identityToken(IdentityToken value) {
    _identityToken = value;
    notifyListeners();
  }
}
