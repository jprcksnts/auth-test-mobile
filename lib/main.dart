import 'package:auth_mobile/bloc/auth_bloc.dart';
import 'package:auth_mobile/bloc/delegate/simple_bloc_delegate.dart';
import 'package:auth_mobile/event/auth_event.dart';
import 'package:auth_mobile/listener/auth_listener.dart';
import 'package:auth_mobile/provider/user_provider.dart';
import 'package:auth_mobile/provider/user_token_provider.dart';
import 'package:auth_mobile/repository/user_repository.dart';
import 'package:auth_mobile/view/login_page.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

void main() {
  BlocSupervisor.delegate = SimpleBlocDelegate();
  final userRepository = UserRepository();

  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => UserProvider()),
        ChangeNotifierProvider(create: (context) => UserTokenProvider()),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider<AuthBloc>(
              create: (context) => AuthBloc(userRepository: userRepository)
                ..add(AppStarted(context: context))),
        ],
        child: MainApp(userRepository: userRepository,),
      ),
    ),
  );
}

class MainApp extends StatefulWidget {
  final UserRepository userRepository;

  const MainApp({Key key, this.userRepository}) : super(key: key);

  @override
  _MainAppState createState() => _MainAppState();
}

class _MainAppState extends State<MainApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: AuthListener(
        child: Center(
          child: LoginPage(
            userRepository: widget.userRepository,
          ),
        ),
      ),
    );
  }
}
