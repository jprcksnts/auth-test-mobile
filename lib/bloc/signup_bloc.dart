import 'dart:convert';
import 'dart:io';

import 'package:auth_mobile/controller/signup_controller.dart';
import 'package:auth_mobile/event/signup_event.dart';
import 'package:auth_mobile/provider/signup_provider.dart';
import 'package:auth_mobile/state/signup_state.dart';
import 'package:auth_mobile/validator/email_validator.dart';
import 'package:bloc/bloc.dart';
import 'package:provider/provider.dart';

class SignupBloc extends Bloc<SignupEvent, SignupState> {
  @override
  SignupState get initialState => SignupInitial();

  @override
  Stream<SignupState> mapEventToState(SignupEvent event) async* {
    if (event is BackButtonPressed) {
      if (event.page > 0)
        yield SignupInitial();
      else
        yield SignupCancelled();
    } else if (event is SignupProceedButtonPressed) {
      SignupState validationState = _validateCredentialsForm(
          event.email, event.password, event.verifyPassword);

      if (validationState != null) {
        yield validationState;
      } else {
        yield SignupProcessing();

        SignupProvider signupProvider =
            Provider.of<SignupProvider>(event.context, listen: false);
        signupProvider.email = event.email;
        signupProvider.password = event.password;

        await Future.delayed(const Duration(milliseconds: 250));

        try {
          var signupEmailAvailabilityResponse =
              await SignupController.checkEmailAvailability(email: event.email);
          var jsonSignupEmailAvailabilityResponse =
              json.decode(signupEmailAvailabilityResponse.body);

          if (signupEmailAvailabilityResponse.statusCode == HttpStatus.ok &&
              jsonSignupEmailAvailabilityResponse['available'] == true) {
            yield SignupProceededToDetails();
          } else {
            String failMessage =
                jsonSignupEmailAvailabilityResponse['message'] +
                    ' ' +
                    jsonSignupEmailAvailabilityResponse['error']['message'];

            yield SignupFailure(message: failMessage);
          }
        } catch (error, stackTrace) {
          yield SignupFailure(
              message:
                  'Failed to check email availability. Please check your internet connection.');
        }
      }
    } else if (event is SignupCreateAccountButtonPressed) {
      yield SignupProcessing();

      SignupState validationState = _validateDetailsform(event.name);
      if (validationState != null) {
        yield validationState;
      } else {
        SignupProvider signupProvider =
            Provider.of<SignupProvider>(event.context, listen: false);
        signupProvider.name = event.name;

        await Future.delayed(const Duration(milliseconds: 250));

        try {
          var signupResponse = await SignupController.signup(
              email: signupProvider.email,
              password: signupProvider.password,
              name: signupProvider.name);
          var jsonSignupResponse = json.decode(signupResponse.body);

          if (signupResponse.statusCode == HttpStatus.ok) {
            yield SignupSuccessful(message: jsonSignupResponse['message']);
          } else {
            String failMessage = jsonSignupResponse['message'] +
                ' ' +
                jsonSignupResponse['error']['message'];

            yield SignupFailure(message: failMessage);
          }
        } catch (error, stackTrace) {
          print(error);
          print(stackTrace);

          yield SignupFailure(
              message: 'Signup failed. Please check your internet connection.');
        }
      }
    }
  }

  SignupState _validateCredentialsForm(
      String email, String password, String verifyPassword) {
    if (!EmailValidator.isEmail(email)) {
      return SignupFailure(message: 'Invalid email format');
    } else if (password.length < 4) {
      return SignupFailure(
          message: 'Password must be at least 4 characters long');
    } else if (verifyPassword.length < 4) {
      return SignupFailure(
          message: 'Verify password must be at least 4 characters long');
    } else if (password != verifyPassword) {
      return SignupFailure(message: 'Passwords do not match');
    }

    return null;
  }

  SignupState _validateDetailsform(String name) {
    if (name.length < 2) {
      return SignupFailure(
          message: 'First name must be at least 2 characters long');
    }

    return null;
  }
}
