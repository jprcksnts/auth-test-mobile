import 'package:auth_mobile/controller/page_router_controller.dart';
import 'package:auth_mobile/event/auth_event.dart';
import 'package:auth_mobile/main.dart';
import 'package:auth_mobile/provider/user_provider.dart';
import 'package:auth_mobile/repository/user_repository.dart';
import 'package:auth_mobile/state/auth_state.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:provider/provider.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final UserRepository userRepository;

  AuthBloc({@required this.userRepository}) : assert(userRepository != null);

  @override
  AuthState get initialState => AuthUninitialized();

  @override
  Stream<AuthState> mapEventToState(
      AuthEvent event,
      ) async* {
    try {
      if (event is AppStarted) {
        final bool hasToken = await userRepository.hasToken(event.context);

        if (hasToken) {
          yield AuthAuthenticatedBySessionResume();
        } else {
          yield AuthUnauthenticated();
        }
      } else if (event is LoggedIn) {
        yield AuthLoading();
        await userRepository.persistToken(event.identityToken);
        yield AuthAuthenticated();
      } else if (event is LoggedOut) {
        yield AuthLoading();
        await userRepository.deleteToken();
        yield AuthUnauthenticated();

        Provider.of<UserProvider>(event.context).user = null;
//        await Provider.of<FirebaseMessagingProvider>(event.context)
//            .firebaseMessaging
//            .deleteInstanceID();
        PageRouterController.pushAndRemoveStack(
          event.context,
          MainApp(
            userRepository: userRepository,
          ),
        );
      }
    } catch (error, exception) {
      print(error);
      print(exception);

      yield AuthUnauthenticated();
    }
  }
}
