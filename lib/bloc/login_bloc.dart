import 'dart:convert';

import 'package:auth_mobile/event/auth_event.dart';
import 'package:auth_mobile/event/login_event.dart';
import 'package:auth_mobile/model/identity_token.dart';
import 'package:auth_mobile/model/user.dart';
import 'package:auth_mobile/provider/user_provider.dart';
import 'package:auth_mobile/provider/user_token_provider.dart';
import 'package:auth_mobile/repository/user_repository.dart';
import 'package:auth_mobile/state/login_state.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:provider/provider.dart';

import 'auth_bloc.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final BuildContext context;
  final UserRepository userRepository;
  final AuthBloc authBloc;

  LoginBloc(
      {@required this.context,
      @required this.userRepository,
      @required this.authBloc})
      : assert(userRepository != null),
        assert(authBloc != null);

  LoginState get initialState => LoginInitial();

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is LoginButtonPressed) {
      yield LoginProcessing();

      await Future.delayed(const Duration(milliseconds: 250));

      try {
        var loginResponse =
            await _attemptLogin(userRepository, event.email, event.password);

        if (loginResponse['data'] != null) {
          User user =
              _setUserFromJsonMap(context, loginResponse['data']['user']);
          IdentityToken identityToken = _setIdentityTokenFromJsonMap(
              context, loginResponse['data']['token']);

          authBloc.add(LoggedIn(identityToken: identityToken));

          yield LoginInitial();
        } else {
          // TODO :: Convert user repository authenticate function to return response instead of class for better handling of content
          print(loginResponse['message']);
          yield LoginFailure(message: 'Login failed.');
        }
      } catch (error, stackTrace) {
        print(error);
        print(stackTrace);

        await Future.delayed(const Duration(seconds: 1));
        yield LoginFailure(
            message: 'Login failed. Please check your internet connection.');
      }
    }
  }
}

_attemptLogin(
    UserRepository userRepository, String email, String password) async {
  final response = await userRepository.authenticate(
    email: email,
    password: password,
  );

  return json.decode(response.body);
}

IdentityToken _setIdentityTokenFromJsonMap(
    BuildContext context, Map jsonToken) {
  IdentityToken identityToken = IdentityToken.fromJson(jsonToken);
  Provider.of<UserTokenProvider>(context, listen: false).identityToken =
      identityToken;

  return identityToken;
}

User _setUserFromJsonMap(BuildContext context, Map jsonUser) {
  User user = User.fromJson(jsonUser);
  Provider.of<UserProvider>(context, listen: false).user = user;

  return user;
}
