import 'package:auth_mobile/event/password_obscure_event.dart';
import 'package:auth_mobile/state/password_obscure_state.dart';
import 'package:bloc/bloc.dart';

class PasswordObscureBloc
    extends Bloc<PasswordObscureEvent, PasswordObscureState> {
  @override
  PasswordObscureState get initialState => PasswordObscureHiddenState();

  @override
  Stream<PasswordObscureState> mapEventToState(
      PasswordObscureEvent event) async* {
    if (event is PasswordObscureHidePressed) {
      yield PasswordObscureHiddenState();
    } else if (event is PasswordObscureShowPressed) {
      yield PasswordObscureShownState();
    }
  }
}
