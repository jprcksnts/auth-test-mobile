import 'dart:convert';

import 'package:auth_mobile/bloc/auth_bloc.dart';
import 'package:auth_mobile/controller/page_router_controller.dart';
import 'package:auth_mobile/controller/user_data_controller.dart';
import 'package:auth_mobile/model/user.dart';
import 'package:auth_mobile/provider/user_provider.dart';
import 'package:auth_mobile/state/auth_state.dart';
import 'package:auth_mobile/view/user_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

class AuthListener extends StatefulWidget {
  final Widget child;

  const AuthListener({Key key, this.child}) : super(key: key);

  @override
  _AuthListenerState createState() => _AuthListenerState();
}

class _AuthListenerState extends State<AuthListener> {
  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthBloc, AuthState>(
      listener: (context, state) {
        if (state is AuthAuthenticatedBySessionResume) {
          _attemptSessionResume();
        } else if (state is AuthAuthenticated) {
          _navigateToUserPage();
        }
      },
      child: widget.child,
    );
  }

  _attemptSessionResume() async {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              const Text('Loading user data..'),
              Container(
                height: 48,
                width: 32,
                padding: EdgeInsets.only(top: 16),
                child: const CircularProgressIndicator(),
              ),
            ],
          ),
        );
      },
    );

    try {
      var response = await UserDataController.getUserData(context);
      if (response != null) {
        var jsonResponse = json.decode(response.body);
        User user = User.fromJson(jsonResponse['data']['user']);
        Provider.of<UserProvider>(context, listen: false).user = user;

        await Future.delayed(const Duration(milliseconds: 500));
        _navigateToUserPage();
      } else {
        throw Exception();
      }
    } catch (error, stackTrace) {
      print(error);
      print(stackTrace);

      Navigator.of(context).pop();
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const Text(
                    'Failed to load user data. Please re-login your account.'),
              ],
            ),
          );
        },
      );
    }
  }

  _navigateToUserPage() {
    PageRouterController.pushAndRemoveStack(context, UserPage());
  }
}
