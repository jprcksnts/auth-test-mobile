import 'package:auth_mobile/bloc/auth_bloc.dart';
import 'package:auth_mobile/controller/api_request_header_controller.dart';
import 'package:auth_mobile/event/auth_event.dart';
import 'package:auth_mobile/model/api_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;

class UserDataController {
  static Future<http.Response> getUserData(BuildContext context) async {
    var response = await http.get(ApiRoute.FETCH_USER_DATA,
        headers: ApiRequestHeaderController.getDefaultHeaderWithToken(context));
    return response;
  }

  static void attemptLogout(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text('Logout'),
          content: const Text('Are you sure you want to logout?'),
          actions: <Widget>[
            RaisedButton(
              color: Colors.red,
              onPressed: () => BlocProvider.of<AuthBloc>(context).add(
                LoggedOut(
                  context: context,
                ),
              ),
              child: const Text(
                'Logout',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
            FlatButton(
              onPressed: () => Navigator.of(context).pop(),
              child: const Text(
                'Cancel',
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
            )
          ],
        );
      },
    );
  }
}
