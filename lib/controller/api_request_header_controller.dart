import 'package:auth_mobile/provider/user_token_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ApiRequestHeaderController {
  static Map<String, String> getDefaultHeader() {
    return {"Content-Type": "application/json", "Accept": "application/json"};
  }

  static Map<String, String> getDefaultHeaderWithToken(BuildContext context) {
    String accessToken =
        Provider.of<UserTokenProvider>(context, listen: false).identityToken.accessToken;

    return {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer " + accessToken
    };
  }
}
