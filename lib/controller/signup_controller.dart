import 'dart:convert';

import 'package:auth_mobile/controller/api_request_header_controller.dart';
import 'package:auth_mobile/model/api_route.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

class SignupController {
  static Future<http.Response> signup(
      {@required String email,
        @required String password,
        @required String name}) async {
    var params = {
      'email': email,
      'password': password,
      'name': name,
    };

    var response = await http.post(ApiRoute.SIGNUP,
        body: json.encode(params),
        headers: ApiRequestHeaderController.getDefaultHeader());
    return response;
  }

  static Future<http.Response> checkEmailAvailability(
      {@required String email}) async {
    var response = await http.get(ApiRoute.SIGNUP_EMAIL_AVAILABILITY(email));
    return response;
  }
}
