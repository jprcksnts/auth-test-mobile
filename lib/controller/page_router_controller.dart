import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PageRouterController {
  static push(BuildContext context, dynamic route,
      {Duration delayDuration}) async {
    try {
      if (delayDuration != null) {
        await Future.delayed(delayDuration);
      }

      if (Platform.isIOS) {
        Navigator.push(context, CupertinoPageRoute(builder: (_) => route));
      } else {
        Navigator.push(context, MaterialPageRoute(builder: (_) => route));
      }
    } catch (e) {
      print(e.toString());
    }
  }

  static pushAndRemoveStack(BuildContext context, dynamic route,
      {Duration delayDuration}) async {
    try {
      if (delayDuration != null) {
        await Future.delayed(delayDuration);
      }

      if (Platform.isIOS) {
        Navigator.pushAndRemoveUntil(
            context,
            CupertinoPageRoute(builder: (_) => route),
                (Route<dynamic> route) => false);
      } else {
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (_) => route),
                (Route<dynamic> route) => false);
      }
    } catch (e) {
      print(e.toString());
    }
  }
}
