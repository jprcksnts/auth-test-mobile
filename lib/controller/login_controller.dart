import 'dart:convert';

import 'package:auth_mobile/controller/api_request_header_controller.dart';
import 'package:auth_mobile/model/api_route.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

class LoginController {
  static Future<http.Response> login(
      {@required String email, @required String password}) async {
    var params = {
      'email': email,
      'password': password,
    };

    var response = await http.post(ApiRoute.LOGIN,
        body: json.encode(params),
        headers: ApiRequestHeaderController.getDefaultHeader());
    return response;
  }
}
