import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class SignupState extends Equatable {
  const SignupState();

  @override
  List<Object> get props => [];
}

class SignupCancelled extends SignupState {}

class SignupInitial extends SignupState {}

class SignupProceededToDetails extends SignupState {}

class SignupProcessing extends SignupState {}

class SignupSuccessful extends SignupState {
  final String message;

  const SignupSuccessful({@required this.message});

  @override
  List<Object> get props => [message];

  @override
  String toString() => 'Signup Successful {message: $message}';
}

class SignupFailure extends SignupState {
  final String message;

  const SignupFailure({@required this.message});

  @override
  List<Object> get props => [message];

  @override
  String toString() => 'Signup Failure {message: $message}';
}
