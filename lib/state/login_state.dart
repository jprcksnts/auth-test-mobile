import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

abstract class LoginState extends Equatable {
  const LoginState();

  @override
  List<Object> get props => [];
}

class LoginInitial extends LoginState {}

class LoginProcessing extends LoginState {}

class LoginFailure extends LoginState {
  final String message;

  const LoginFailure({@required this.message});

  @override
  List<Object> get props => [message];

  @override
  String toString() => 'Login Failure {error: $message}';
}