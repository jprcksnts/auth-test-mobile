import 'package:equatable/equatable.dart';

abstract class PasswordObscureState extends Equatable {
  @override
  List<Object> get props => [];
}

class PasswordObscureHiddenState extends PasswordObscureState {}

class PasswordObscureShownState extends PasswordObscureState {}