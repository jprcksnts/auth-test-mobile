class ApiRoute {
  static const _LOCAL = true;

  static const HOST = (_LOCAL)
      ? "http://192.168.1.10:8000/"
      : "http://auth-web.test/";
  static const API = HOST + "api/";

  /// Auth Routes
  static const LOGIN = API + "user/login";

  static SIGNUP_EMAIL_AVAILABILITY(email) =>
      API + 'user/email/$email/availability';
  static const SIGNUP = API + "user/signup";

  static const FETCH_USER_DATA = API + 'user';
}
