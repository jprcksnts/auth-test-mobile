import 'package:json_annotation/json_annotation.dart';

part 'identity_token.g.dart';

@JsonSerializable()
class IdentityToken {
  @JsonKey(name: 'access_token')
  String accessToken;
  @JsonKey(name: 'refresh_token')
  String refreshToken;
  @JsonKey(name: 'expires_at')
  DateTime expiresAt;

  IdentityToken();

  factory IdentityToken.fromJson(Map<String, dynamic> json) =>
      _$IdentityTokenFromJson(json);

  Map<String, dynamic> toJson() => _$IdentityTokenToJson(this);
}
