// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'identity_token.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

IdentityToken _$IdentityTokenFromJson(Map<String, dynamic> json) {
  return IdentityToken()
    ..accessToken = json['access_token'] as String
    ..refreshToken = json['refresh_token'] as String
    ..expiresAt = json['expires_at'] == null
        ? null
        : DateTime.parse(json['expires_at'] as String);
}

Map<String, dynamic> _$IdentityTokenToJson(IdentityToken instance) =>
    <String, dynamic>{
      'access_token': instance.accessToken,
      'refresh_token': instance.refreshToken,
      'expires_at': instance.expiresAt?.toIso8601String(),
    };
