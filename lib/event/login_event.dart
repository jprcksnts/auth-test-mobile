import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class LoginEvent extends Equatable {
  const LoginEvent();
}

class LoginButtonPressed extends LoginEvent {
  final BuildContext context;
  final String email;
  final String password;

  const LoginButtonPressed({
    @required this.context,
    @required this.email,
    @required this.password,
  });

  @override
  List<Object> get props => [context, email, password];

  @override
  String toString() =>
      'Login Button Pressed {username: $email, password: $password }';
}
