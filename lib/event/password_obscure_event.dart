import 'package:equatable/equatable.dart';

abstract class PasswordObscureEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class PasswordObscureShowPressed extends PasswordObscureEvent {}

class PasswordObscureHidePressed extends PasswordObscureEvent {}