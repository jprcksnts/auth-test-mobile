import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class SignupEvent extends Equatable {
  const SignupEvent();
}

class BackButtonPressed extends SignupEvent {
  final int page;

  BackButtonPressed({@required this.page});

  @override
  List<Object> get props => [];
}

class SignupProceedButtonPressed extends SignupEvent {
  final BuildContext context;
  final String email;
  final String password;
  final String verifyPassword;

  SignupProceedButtonPressed(
      {@required this.context,
        @required this.email,
        @required this.password,
        @required this.verifyPassword});

  @override
  List<Object> get props => [email, password, verifyPassword];

  @override
  String toString() {
    return 'SignupProceedButtonPressed {email: $email, password: $password, verifyPassword: $verifyPassword}';
  }
}

class SignupCreateAccountButtonPressed extends SignupEvent {
  final BuildContext context;
  final String name;

  SignupCreateAccountButtonPressed(
      {@required this.context,
        @required this.name,});

  @override
  List<Object> get props => [name];

  @override
  String toString() {
    return 'SignupButtonPressed {name: $name}';
  }
}
