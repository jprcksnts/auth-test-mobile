import 'package:auth_mobile/model/identity_token.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();

  @override
  List<Object> get props => [];
}

class AppStarted extends AuthEvent {
  final BuildContext context;
  const AppStarted({@required this.context});
}

class LoggedIn extends AuthEvent {
  final IdentityToken identityToken;

  const LoggedIn({@required this.identityToken});

  @override
  List<Object> get props => [identityToken];

  @override
  String toString() => 'Logged In {identityToken: $identityToken}';
}

class LoggedOut extends AuthEvent {
  final BuildContext context;

  LoggedOut({@required this.context});
}