import 'package:flutter/material.dart';

class CircleIconButton extends StatelessWidget {
  final double size;
  final VoidCallback onPressed;
  final IconData icon;
  final Color iconColor;
  final Color backgroundColor;
  final EdgeInsets padding;

  CircleIconButton(
      {this.size = 32,
        @required this.onPressed,
        @required this.icon,
        this.iconColor = Colors.blue,
        this.backgroundColor = Colors.white,
        this.padding = const EdgeInsets.all(8)});

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(size * 2),
      child: Material(
        color: backgroundColor,
        child: InkWell(
          onTap: this.onPressed,
          child: Padding(
            padding: padding,
            child: Icon(
              icon,
              color: iconColor,
              size: size * 0.66, // 66% width for icon
            ),
          ),
        ),
      ),
    );
  }
}