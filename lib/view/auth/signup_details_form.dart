import 'package:auth_mobile/bloc/signup_bloc.dart';
import 'package:auth_mobile/event/signup_event.dart';
import 'package:auth_mobile/state/signup_state.dart';
import 'package:auth_mobile/view/auth/signup_details_header.dart';
import 'package:auth_mobile/view/widget/page_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SignupDetailsForm extends StatefulWidget {
  const SignupDetailsForm();

  @override
  _SignupDetailsFormState createState() => _SignupDetailsFormState();
}

class _SignupDetailsFormState extends State<SignupDetailsForm>
    with AutomaticKeepAliveClientMixin {
  TextEditingController _tecName = TextEditingController();

  @override
  bool get wantKeepAlive => true;

  @override
  void dispose() {
    super.dispose();
    _tecName.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SingleChildScrollView(
      child: PageContainer(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            const SignupDetailsHeader(),
            const SizedBox(
              height: 16,
            ),
            const Text(
              'User Name',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            TextFormField(
              controller: _tecName,
              decoration: const InputDecoration(
                labelText: "Name",
                hintText: "Enter Name",
              ),
            ),
            BlocBuilder<SignupBloc, SignupState>(
              builder: (context, state) {
                if (state is SignupFailure)
                  return Container(
                    padding: const EdgeInsets.only(top: 16),
                    child: Text(
                      state.message,
                      style: const TextStyle(
                        color: Colors.red,
                      ),
                    ),
                  );
                else if (state is SignupSuccessful) {
                  return Container(
                    padding: const EdgeInsets.only(top: 16),
                    child: Text(
                      state.message,
                      style: const TextStyle(
                        color: Colors.green,
                      ),
                    ),
                  );
                } else {
                  return const SizedBox(
                    height: 16,
                  );
                }
              },
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 16,
                bottom: 16,
              ),
              child: BlocBuilder<SignupBloc, SignupState>(
                builder: (context, state) {
                  return Align(
                    alignment: Alignment.centerRight,
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left: 8, right: 16),
                          child: Container(
                            height: 24,
                            width: 24,
                            child: (state is SignupProcessing)
                                ? const CircularProgressIndicator()
                                : null,
                          ),
                        ),
                        RaisedButton(
                          color: Colors.blue,
                          onPressed: (state is SignupProcessing ||
                                  state is SignupSuccessful)
                              ? null
                              : _attemptCreateAccount,
                          child: const Text(
                            "Create Account",
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  _attemptCreateAccount() {
    BlocProvider.of<SignupBloc>(context).add(
      SignupCreateAccountButtonPressed(
        context: context,
        name: _tecName.text,
      ),
    );
  }
}
