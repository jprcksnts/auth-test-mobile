import 'package:auth_mobile/bloc/password_obscure_bloc.dart';
import 'package:auth_mobile/bloc/signup_bloc.dart';
import 'package:auth_mobile/event/password_obscure_event.dart';
import 'package:auth_mobile/event/signup_event.dart';
import 'package:auth_mobile/state/password_obscure_state.dart';
import 'package:auth_mobile/state/signup_state.dart';
import 'package:auth_mobile/view/auth/signup_credentials_header.dart';
import 'package:auth_mobile/view/widget/circle_icon_button.dart';
import 'package:auth_mobile/view/widget/page_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SignupCredentialsForm extends StatefulWidget {
  const SignupCredentialsForm();

  @override
  _SignupCredentialsFormState createState() => _SignupCredentialsFormState();
}

class _SignupCredentialsFormState extends State<SignupCredentialsForm>
    with AutomaticKeepAliveClientMixin {
  final TextEditingController _tecEmail = TextEditingController();
  final TextEditingController _tecPassword = TextEditingController();
  final TextEditingController _tecVerifyPassword = TextEditingController();

  final PasswordObscureBloc _passwordObscureBloc = PasswordObscureBloc();
  final PasswordObscureBloc _verifyPasswordObscureBloc = PasswordObscureBloc();

  @override
  bool get wantKeepAlive => true;

  @override
  void dispose() {
    super.dispose();
    _tecEmail.dispose();
    _tecPassword.dispose();
    _tecVerifyPassword.dispose();

    _passwordObscureBloc.close();
    _verifyPasswordObscureBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SingleChildScrollView(
      child: PageContainer(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            const SignupCredentialsHeader(),
            TextFormField(
              controller: _tecEmail,
              decoration: const InputDecoration(
                labelText: "Email",
                hintText: "Enter email",
              ),
            ),
            BlocProvider<PasswordObscureBloc>(
              create: (context) => _passwordObscureBloc,
              child: BlocBuilder<PasswordObscureBloc, PasswordObscureState>(
                builder: (context, state) => TextFormField(
                  controller: _tecPassword,
                  decoration: InputDecoration(
                    labelText: "Password",
                    hintText: "Enter password",
                    suffixIcon: CircleIconButton(
                      icon: Icons.remove_red_eye,
                      onPressed: () => (state is PasswordObscureHiddenState)
                          ? _passwordObscureBloc
                          .add(PasswordObscureShowPressed())
                          : _passwordObscureBloc
                          .add(PasswordObscureHidePressed()),
                    ),
                  ),
                  obscureText:
                  (state is PasswordObscureHiddenState) ? true : false,
                ),
              ),
            ),
            BlocProvider<PasswordObscureBloc>(
              create: (context) => _verifyPasswordObscureBloc,
              child: BlocBuilder<PasswordObscureBloc, PasswordObscureState>(
                builder: (context, state) => TextFormField(
                  controller: _tecVerifyPassword,
                  decoration: InputDecoration(
                    labelText: "Verify Password",
                    hintText: "Re-enter password",
                    suffixIcon: CircleIconButton(
                      icon: Icons.remove_red_eye,
                      onPressed: () => (state is PasswordObscureHiddenState)
                          ? _verifyPasswordObscureBloc
                          .add(PasswordObscureShowPressed())
                          : _verifyPasswordObscureBloc
                          .add(PasswordObscureHidePressed()),
                    ),
                  ),
                  obscureText:
                  (state is PasswordObscureHiddenState) ? true : false,
                ),
              ),
            ),
            BlocBuilder<SignupBloc, SignupState>(
              builder: (context, state) {
                if (state is SignupFailure) {
                  return Container(
                    padding: const EdgeInsets.only(top: 16),
                    child: Text(
                      state.message,
                      style: const TextStyle(
                        color: Colors.red,
                      ),
                    ),
                  );
                }

                return const SizedBox(
                  height: 16,
                );
              },
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 16,
                bottom: 16,
              ),
              child: BlocBuilder<SignupBloc, SignupState>(
                builder: (context, state) {
                  return Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 8, right: 16),
                        child: Container(
                          height: 24,
                          width: 24,
                          child: (state is SignupProcessing)
                              ? const CircularProgressIndicator()
                              : null,
                        ),
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: RaisedButton(
                          color: Colors.blue,
                          onPressed: (state is SignupProcessing)
                              ? null
                              : _proceedToNextForm,
                          child: const Text(
                            'Next',
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ],
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  _proceedToNextForm() {
    BlocProvider.of<SignupBloc>(context).add(
      SignupProceedButtonPressed(
        context: context,
        email: _tecEmail.text,
        password: _tecPassword.text,
        verifyPassword: _tecVerifyPassword.text,
      ),
    );
  }
}
