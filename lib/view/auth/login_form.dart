import 'package:auth_mobile/bloc/login_bloc.dart';
import 'package:auth_mobile/bloc/password_obscure_bloc.dart';
import 'package:auth_mobile/event/login_event.dart';
import 'package:auth_mobile/event/password_obscure_event.dart';
import 'package:auth_mobile/state/login_state.dart';
import 'package:auth_mobile/state/password_obscure_state.dart';
import 'package:auth_mobile/view/widget/circle_icon_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginForm extends StatefulWidget {
  const LoginForm();

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final TextEditingController _tecEmail = TextEditingController();
  final TextEditingController _tecPassword = TextEditingController();
  final PasswordObscureBloc _passwordObscureBloc = PasswordObscureBloc();

  @override
  void dispose() {
    super.dispose();
    _tecEmail.dispose();
    _tecPassword.dispose();
    _passwordObscureBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Card(
          child: Padding(
            padding: const EdgeInsets.all(16),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8),
                  child: const Text(
                    'Login',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                const Divider(),
                TextFormField(
                  controller: _tecEmail,
                  decoration: const InputDecoration(
                    labelText: 'Email',
                    hintText: 'Enter email',
                  ),
                ),
                BlocProvider<PasswordObscureBloc>(
                  create: (context) => _passwordObscureBloc,
                  child: BlocBuilder<PasswordObscureBloc, PasswordObscureState>(
                    builder: (context, state) => TextFormField(
                      controller: _tecPassword,
                      decoration: InputDecoration(
                        labelText: "Password",
                        hintText: "Enter password",
                        suffixIcon: CircleIconButton(
                          icon: Icons.remove_red_eye,
                          onPressed: () => (state is PasswordObscureHiddenState)
                              ? _passwordObscureBloc
                                  .add(PasswordObscureShowPressed())
                              : _passwordObscureBloc
                                  .add(PasswordObscureHidePressed()),
                        ),
                      ),
                      obscureText:
                          (state is PasswordObscureHiddenState) ? true : false,
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 16),
                    child: BlocBuilder<LoginBloc, LoginState>(
                      builder: (context, state) {
                        return Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(
                                left: 8,
                                right: 16,
                              ),
                              child: Container(
                                height: 24,
                                width: 24,
                                child: (state is LoginProcessing)
                                    ? const CircularProgressIndicator()
                                    : null,
                              ),
                            ),
                            RaisedButton(
                              color: Colors.blue,
                              child: const Text(
                                'Login',
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                              onPressed: (state is LoginProcessing)
                                  ? null
                                  : _attemptLogin,
                            ),
                          ],
                        );
                      },
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        BlocBuilder<LoginBloc, LoginState>(
          builder: (context, state) {
            if (state is LoginFailure) {
              return Container(
                padding: const EdgeInsets.only(
                  top: 16,
                  left: 8,
                  right: 8,
                ),
                child: Text(
                  state.message,
                  style: const TextStyle(
                    color: Colors.red,
                  ),
                ),
              );
            }

            return const SizedBox(
              height: 16,
            );
          },
        ),
      ],
    );
  }

  _attemptLogin() {
    BlocProvider.of<LoginBloc>(context).add(
      LoginButtonPressed(
        context: context,
        email: _tecEmail.text,
        password: _tecPassword.text,
      ),
    );
  }
}
