import 'package:flutter/material.dart';

class SignupCredentialsHeader extends StatelessWidget {
  const SignupCredentialsHeader();

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const Text(
          'User Credentials',
          style: TextStyle(
            fontSize: 24,
            fontWeight: FontWeight.bold,
          ),
        ),
        const Text(
          'Provided details will be used for logging in to your account.',
          style: TextStyle(
            fontSize: 12,
          ),
        ),
        const SizedBox(
          height: 16,
        ),
      ],
    );
  }
}
