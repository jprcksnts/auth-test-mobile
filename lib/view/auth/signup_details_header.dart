import 'package:flutter/material.dart';

class SignupDetailsHeader extends StatelessWidget {
  const SignupDetailsHeader();

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const Text(
          'User Details',
          style: TextStyle(
            fontSize: 24,
            fontWeight: FontWeight.bold,
          ),
        ),
        const Text(
          'Provided details will be used when processing reservations and orders. '
              'Please provide the correct information to avoid having issues when receiving reserved or ordered items.',
          style: TextStyle(
            fontSize: 12,
          ),
        ),
        const SizedBox(
          height: 16,
        ),
      ],
    );
  }
}
