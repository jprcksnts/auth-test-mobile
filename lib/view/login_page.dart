import 'package:auth_mobile/bloc/auth_bloc.dart';
import 'package:auth_mobile/bloc/login_bloc.dart';
import 'package:auth_mobile/controller/page_router_controller.dart';
import 'package:auth_mobile/repository/user_repository.dart';
import 'package:auth_mobile/view/auth/login_form.dart';
import 'package:auth_mobile/view/signup_page.dart';
import 'package:auth_mobile/view/widget/page_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginPage extends StatefulWidget {
  final UserRepository userRepository;

  const LoginPage({Key key, @required this.userRepository})
      : assert(userRepository != null),
        super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: BlocProvider(
          create: (context) => LoginBloc(
            context: context,
            authBloc: BlocProvider.of<AuthBloc>(context),
            userRepository: widget.userRepository,
          ),
          child: Center(
            child: SingleChildScrollView(
              child: PageContainer(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    const LoginForm(),
                    Padding(
                      padding: const EdgeInsets.only(top: 16, bottom: 16),
                      child: Wrap(
                        crossAxisAlignment: WrapCrossAlignment.center,
                        children: <Widget>[
                          const Text("Don't have an account yet?"),
                          GestureDetector(
                            behavior: HitTestBehavior.opaque,
                            onTap: () => PageRouterController.push(
                                context, const SignupPage()),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 8, horizontal: 4),
                              child: const Text(
                                "Signup now!",
                                style: TextStyle(
                                  color: Colors.blue,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
