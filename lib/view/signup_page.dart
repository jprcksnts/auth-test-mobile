import 'package:auth_mobile/bloc/signup_bloc.dart';
import 'package:auth_mobile/event/signup_event.dart';
import 'package:auth_mobile/provider/signup_provider.dart';
import 'package:auth_mobile/state/signup_state.dart';
import 'package:auth_mobile/view/auth/signup_credentials_form.dart';
import 'package:auth_mobile/view/auth/signup_details_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

class SignupPage extends StatefulWidget {
  const SignupPage();

  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  SignupBloc _signupBloc = SignupBloc();
  PageController _pageController = PageController();

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
    _signupBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: ChangeNotifierProvider<SignupProvider>(
        create: (context) => SignupProvider(),
        child: BlocProvider<SignupBloc>(
          create: (context) => _signupBloc,
          child: BlocListener<SignupBloc, SignupState>(
            listener: (context, state) {
              if (state is SignupCancelled) {
                _closeSignup();
              } else if (state is SignupSuccessful) {
                _closeSignup(delay: 1000);
              } else if (state is SignupInitial) {
                _returnToCredentials();
              } else if (state is SignupProceededToDetails) {
                _proceedToDetails();
              }
            },
            child: Scaffold(
              appBar: AppBar(
                title: const Text("Signup"),
              ),
              body: SafeArea(
                child: PageView(
                  controller: _pageController,
                  physics: const NeverScrollableScrollPhysics(),
                  children: <Widget>[
                    const SignupCredentialsForm(),
                    const SignupDetailsForm(),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<bool> _onWillPop() async {
    int page = _pageController.page.toInt();

    _signupBloc.add(
      BackButtonPressed(page: page),
    );

    return false;
  }

  _closeSignup({int delay = 0}) async {
    await Future.delayed(Duration(milliseconds: delay));
    Navigator.of(context).pop();
  }

  _returnToCredentials() {
    _pageController.animateToPage(0,
        duration: const Duration(milliseconds: 250), curve: Curves.easeIn);
  }

  _proceedToDetails() {
    _pageController.animateToPage(1,
        duration: const Duration(milliseconds: 250), curve: Curves.easeIn);
  }
}
