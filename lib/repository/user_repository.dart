import 'dart:convert';

import 'package:auth_mobile/controller/login_controller.dart';
import 'package:auth_mobile/model/identity_token.dart';
import 'package:auth_mobile/provider/user_token_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

class UserRepository {
  Future<http.Response> authenticate({
    @required String email,
    @required String password,
  }) async {
    var response =
        await LoginController.login(email: email, password: password);

    return response;
  }

  Future<void> deleteToken() async {
    final storage = FlutterSecureStorage();
    await storage.delete(key: 'com.capstone.pmd_mobile');
    return;
  }

  Future<void> persistToken(IdentityToken identityToken) async {
    final storage = FlutterSecureStorage();
    storage.write(
        key: 'com.capstone.pmd_mobile', value: json.encode(identityToken));
  }

  Future<bool> hasToken(BuildContext context) async {
    final storage = FlutterSecureStorage();
    final String identityTokenString =
        await storage.read(key: 'com.capstone.pmd_mobile');

    if (identityTokenString != null) {
      Provider.of<UserTokenProvider>(context, listen: false).identityToken =
          IdentityToken.fromJson(json.decode(identityTokenString));

      return true;
    }

    return false;
  }
}
